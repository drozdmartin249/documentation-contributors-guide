
= Building a local preview
Fedora Documentation Team <https://discussion.fedoraproject.org/tag/docs>
v0.0.1, 2022-09-26

Because the documentation site is using a markup language instead of a WYSIWYG editor, the sources do not look exactly like the rendered page will. Therefore it is necessary to check that your changes look the way you want them to before you push and make a pull request. This involves building a local preview.

There are two shell scripts available in each existing repository (including the template repository used to create new content): `build.sh` and `preview.sh`. 

To preview, run the `build.sh` script; this is the one that actually builds a local version of the site (or, more precisely, the subset of the full site that resides in your current repository). Then, run `preview.sh`, which starts a webserver and serves the site at link:http://localhost:8080/[]. Opening this URL in any web browser will show you the preview, which will be available until you kill the process (kbd:[Ctrl+C] in the terminal).

[NOTE]
====
To run the scripts you'll need to install [application]`Podman` if using a Linux OS such as Fedora or [application]`Docker CE` if using macOS. 
====

== Previewing multiple repositories
If your work spans content in multiple repositories,
e.g. because you xref:contributing-docs/asciidoc-markup.adoc#external-antora-link[link to another repository],
you can extend the preview by adding more repositories to `site.yml` as follows:

----
content:
  sources:
  - url: .
    branches: HEAD
  - url: https://pagure.io/fedora-docs/another-repository.git
    branches: main
----

Correct entries to use can be found from
https://pagure.io/fedora-docs/docs-fp-o/blob/prod/f/site.yml[docs-fp-o site.yml].

== Using the regular Antora scripts
If you want to use the regular Antora build and preview workflow - follow the instructions on https://docs.antora.org/[Antora Documentation page]. 

Once you have ``Antora CLI`` and ``Antora Site Generator`` you can build and preview the pages without the container scripts. 

To build the pages in the project directory run:

[source]
----
antora generate site.yml
----

This will create a new directory ``public`` which contains all the necessary files. Navigate there and run a server command. You might already have a Python simple server, in which case run:

[source]
----
python3 -m http.server
----

or if you only have Python 2 on your machine:

[source]
----
python -m SimpleHTTPServer
----

It opens a local preview at port 8000.

If you have cargo (Rust package manager), you could also install and use ``miniserve`` or any other simple server of your choice for that matter. 
